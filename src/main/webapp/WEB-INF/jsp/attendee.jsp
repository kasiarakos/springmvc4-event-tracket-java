<%--
  Created by IntelliJ IDEA.
  User: dimitriskasiaras
  Date: 2020-02-03
  Time: 15:07
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="f" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="sp" uri="http://www.springframework.org/tags" %>

<html>
<head>
    <title>Attendee Page</title>
    <style type="text/css">

        .error{
            color: #ff0000;
        }
        .error-block{
            color: #000;
            background-color: #ffeeee;
            border: 3px solid #ff0000;
            padding: 8px;
            margin: 16px;
        }
    </style>
</head>
<body>
<a href="?lang=en">English</a> | <a href="?lang=el">Ελληνικά</a>
<br />
<f:form modelAttribute="attendee">
    <f:errors path="*" cssClass="error-block" element="div" />
    <label for="textinput1"><sp:message code="attendee.name" />:</label>
    <f:input path="name" cssErrorClass="error" id="textinput1"/>
    <f:errors path="name" cssClass="error" />
    <br />
    <label for="textinput2"><sp:message code="attendee.email.address" />:</label>
    <f:input path="emailAddress" cssErrorClass="error" id="textinput2"/>
    <f:errors path="emailAddress" cssClass="error" />
    <input type="submit" class="button" value="enter Attendee">
    <br />
    <label for="textinput3"><sp:message code="attendee.phone" />:</label>
    <f:input path="phone" cssErrorClass="error" id="textinput3"/>
    <f:errors path="phone" cssClass="error" />
    <input type="submit" class="button" value="enter Attendee">
</f:form>
</body>
</html>
