<%--
  Created by IntelliJ IDEA.
  User: dimitriskasiaras
  Date: 2020-02-03
  Time: 15:07
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="f" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
    <title>Title</title>
    <style type="text/css">

        .error{
            color: #ff0000;
        }
        .error-block{
            color: #000;
            background-color: #ffeeee;
            border: 3px solid #ff0000;
            padding: 8px;
            margin: 16px;
        }
    </style>
</head>
<body>
    <f:form modelAttribute="event">
        <f:errors path="*" cssClass="error-block" element="div" />
        <label for="textinput1">Minutes: </label>
        <f:input path="name" cssErrorClass="error" id="textinput1"/>
        <f:errors path="name" cssClass="error" />
        <input type="submit" class="button" value="enter event">

    </f:form>
</body>
</html>
