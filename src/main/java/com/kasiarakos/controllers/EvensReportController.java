package com.kasiarakos.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.kasiarakos.domain.Event;

@RestController
@RequestMapping(value = "/events",
    produces={
        MediaType.APPLICATION_XML_VALUE,
        MediaType.APPLICATION_JSON_VALUE
    })
public class EvensReportController {

    @GetMapping
    public Event[] getEvens(){



        Event event = new Event();
        event.setName("event1");

        Event event2 = new Event();
        event2.setName("event2");

        Event[] events = {event, event2};
        return events;
    }


    @GetMapping("/myEvent")
    public Event getEvent(){


        Event event = new Event();
        event.setName("event1");

        return event;
    }
}
