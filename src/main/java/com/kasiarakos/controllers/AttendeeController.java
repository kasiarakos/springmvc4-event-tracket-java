package com.kasiarakos.controllers;

import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import com.kasiarakos.domain.Attendee;

@Controller
@RequestMapping("/attendee")
public class AttendeeController {

    @GetMapping
    public String getAttendee(Model model){
        Attendee attendee = new Attendee();

        model.addAttribute("attendee", attendee);
        return "attendee";
    }

    @PostMapping
    public String postAttendee(@Valid @ModelAttribute Attendee attendee, BindingResult result){
        System.out.println(attendee);
        if(result.hasErrors()){
            return "attendee";
        }
        return "redirect:index.html";
    }
}
