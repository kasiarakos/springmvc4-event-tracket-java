package com.kasiarakos.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import com.kasiarakos.domain.Event;

@Controller
@RequestMapping("/event")
@SessionAttributes("event")
public class EventController {

    @GetMapping
    public String getEvent(Model model) {
        Event event = new Event();
        event.setName("Java User Group");

        model.addAttribute("event", event);
        return "event";
    }

    @PostMapping
    public String postEvent(@ModelAttribute("event") Event event ) {
        System.out.println(event);
        return "redirect:index.html";
    }
}
