package com.kasiarakos.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HelloController {

    @GetMapping("/hello")
    public String hello(Model model){
        model.addAttribute("greeting", "Hello Kasiarakos");
        return "hello";
    }

    @GetMapping("index")
    public String getIndex(){
        return "forward:index.jsp";
    }
}
